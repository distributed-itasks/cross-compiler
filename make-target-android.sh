(cd source ; sh ../get-sources.sh)

mkdir -p target/android

cp -r tools/* target/android
cp -r source/* target/android

cp -r clean-cross target/android/clean-cross
(cd target/android/clean-cross ; make)

cp -r support/Engine.* target/android/iTasks-SDK/Server/iTasks/_Framework
