(cd "compiler"; ${CLM} -IL ${CLEAN_STDLIB} -ABC -nw -ci -I backend -I frontend -I main -I main/Unix -IL ArgEnv backendconvert)
if [ "$TARGET" = "MACOS" ]; then
	(cd "compiler"; export CLM="${CLM} -I ${CLEAN_STDLIB}"; sh $PATCH_MAKE/compiler/make.macosx64.sh)
elif [ "$CROSS_PLATFORM" = "LINUX64" ]; then
	(cd "compiler"; export CLM="${CLM} -I ${CLEAN_STDLIB}"; sh $PATCH_MAKE/compiler/make.linux64.sh)
else
	(cd "compiler"; export CLM="${CLM} -I ${CLEAN_STDLIB}"; sh $PATCH_MAKE/compiler/make.linux.sh)
fi
