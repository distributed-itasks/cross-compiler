echo " ---- BUILD STDENV ---- "

mkdir -p clean/StdEnv

cp libraries/StdEnv/_library.dcl clean/StdEnv
cp libraries/StdEnv/_startup.dcl clean/StdEnv
cp libraries/StdEnv/_system.dcl clean/StdEnv
for a in StdArray StdBool StdChar StdCharList StdClass StdDebug StdEnum StdEnv \
	 StdFunc StdList StdMisc StdOrdList StdOverloaded StdOverloadedList \
	 StdStrictLists StdTuple _SystemArray _SystemEnum _SystemEnumStrict \
	 _SystemStrictLists StdGeneric;
do cp libraries/StdEnv/$a.[di]cl clean/StdEnv ;
done

if [ "$CROSS_PLATFORM" = "MACOS" ]; then
	cp libraries/StdEnv/StdInt.icl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdInt.dcl clean/StdEnv
	cp libraries/StdEnv/StdFile.dcl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdFile.icl clean/StdEnv
	cp libraries/StdEnv/StdReal.dcl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdReal.icl clean/StdEnv
	cp libraries/StdEnv/StdString.dcl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdString.icl clean/StdEnv
elif [ "$CROSS_PLATFORM" = "LINUX64" ]; then
	cp libraries/StdEnv/StdInt.icl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdInt.dcl clean/StdEnv
	cp libraries/StdEnv/StdFile.dcl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdFile.icl clean/StdEnv
	cp libraries/StdEnv/StdReal.dcl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdReal.icl clean/StdEnv
	cp libraries/StdEnv/StdString.dcl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdString.icl clean/StdEnv
elif [ "$CROSS_PLATFORM" = "WIN64" ]; then
	cp libraries/StdEnv/StdInt.icl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdInt.dcl clean/StdEnv
	cp libraries/StdEnv/StdFile.dcl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdFile.icl clean/StdEnv
	cp libraries/StdEnv/StdReal.dcl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdReal.icl clean/StdEnv
	cp libraries/StdEnv/StdString.dcl clean/StdEnv
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/StdString.icl clean/StdEnv
else
	cp libraries/StdEnv/StdInt.icl clean/StdEnv
	cp libraries/StdEnv/StdInt.dcl clean/StdEnv
	cp libraries/StdEnv/StdFile.icl clean/StdEnv
	cp libraries/StdEnv/StdFile.dcl clean/StdEnv
	cp libraries/StdEnv/StdReal.icl clean/StdEnv
	cp libraries/StdEnv/StdReal.dcl clean/StdEnv
	cp libraries/StdEnv/StdString.icl clean/StdEnv
	cp libraries/StdEnv/StdString.dcl clean/StdEnv
fi

if [ "$CROSS_PLATFORM" = "MACOS" ]; then
	cp tools/build/MacOSX/txt/_startupProfile.dcl clean/StdEnv
	cp tools/build/MacOSX/txt/_startupTrace.dcl clean/StdEnv
	cp tools/build/MacOSX/txt/Makefile_stdenv clean/StdEnv
	cp tools/build/MacOSX/txt/make.sh clean/StdEnv/make.sh
	cp tools/build/MacOSX/txt/install.sh clean/StdEnv/install.sh
	cp tools/build/MacOSX/txt/install_stdenv.sh clean/StdEnv/install_stdenv.sh
else
	cp tools/build/Linux/txt/_startupProfile.dcl clean/StdEnv
	cp tools/build/Linux/txt/_startupTrace.dcl clean/StdEnv
	cp tools/build/Linux/txt/Makefile_stdenv clean/StdEnv/Makefile
	cp tools/build/Linux/txt/make.sh clean/StdEnv/make.sh
	if [ "$CROSS_PLATFORM" = "WIN32" ]; then
		cp $PATCH_MAKE/txt/win/install.sh clean/StdEnv/install.sh
	elif [ "$CROSS_PLATFORM" = "WIN64" ]; then
		cp $PATCH_MAKE/txt/win/install.sh clean/StdEnv/install.sh
	else
		cp tools/build/Linux/txt/install.sh clean/StdEnv/install.sh
	fi
	cp tools/build/Linux/txt/install_stdenv.sh clean/StdEnv/install_stdenv.sh
fi

mkdir -p clean/StdEnv/Clean\ System\ Files
if [ "$CROSS_PLATFORM" = "MACOS" ]; then
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/_system.abc clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/macho64/_startup.o clean/StdEnv/Clean\ System\ Files
        cp runtimesystem/macho64/_startupTrace.o clean/StdEnv/Clean\ System\ Files
        cp runtimesystem/macho64/_startupProfile.o clean/StdEnv/Clean\ System\ Files
elif [ "$CROSS_PLATFORM" = "WIN32" ]; then
	cp libraries/StdEnv/Clean\ System\ Files/_system.abc clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startup0.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startup1.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startup2.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startupTrace.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startupProfile.o clean/StdEnv/Clean\ System\ Files
elif [ "$CROSS_PLATFORM" = "WIN64" ]; then
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/_system.abc clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startup0.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startup1.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startup2.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startupTrace.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startupProfile.o clean/StdEnv/Clean\ System\ Files
elif [ "$CROSS_PLATFORM" = "LINUX64" ]; then
	cp libraries/StdEnv/StdEnv\ 64\ Changed\ Files/_system.abc clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/linux64/_startup.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/linux64Trace/_startupTrace.o clean/StdEnv/Clean\ System\ Files	
else
	cp libraries/StdEnv/Clean\ System\ Files/_system.abc clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startup.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startupTrace.o clean/StdEnv/Clean\ System\ Files
	cp runtimesystem/_startupProfile.o clean/StdEnv/Clean\ System\ Files
fi

$STDENV_CLEAN_HOME/exe/cg clean/StdEnv/Clean\ System\ Files/_system

echo first compile of system modules
for a in StdChar; # compile twice for inlining
do $STDENV_CLEAN_HOME/exe/cocl -P clean/StdEnv $a ;
done

echo second compile of system modules
for a in StdMisc StdBool StdInt StdChar StdFile StdReal StdString;
do $STDENV_CLEAN_HOME/exe/cocl -P clean/StdEnv $a ;
done

