CUR_DIR=$(pwd)

export CLEAN_HOME=$CUR_DIR/clean-cross
export CLM=$CLEAN_HOME/bin/clm
export CLEAN_STDLIB=$CLEAN_HOME/lib/StdLib

export STDENV_CLEAN_HOME=$CLEAN_HOME # Default use folder clean (compiled compiler).

export PATCH_MAKE=$CUR_DIR/make

export TARGET=ARMCROSS

# ANDROID or RASPBERRYPI
export CROSS_PLATFORM=RASPBERRYPI

# DLL or EXE
export TARGETTYPE=EXE

export CC=/usr/bin/gcc
export AS=as
export LD=/usr/bin/ld
export CFLAGS=''

export CROSS_CC=/opt/raspberrypi/tools/arm-bcm2708/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-gcc
export CROSS_AS=/opt/raspberrypi/tools/arm-bcm2708/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-as
export CROSS_LD=/opt/raspberrypi/tools/arm-bcm2708/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-ld
export CROSS_CFLAGS='--sysroot=/opt/raspberrypi/tools/arm-bcm2708/arm-bcm2708hardfp-linux-gnueabi/arm-bcm2708hardfp-linux-gnueabi/sysroot'
export CROSS_ASFLAGS=""

sh build-target.sh

