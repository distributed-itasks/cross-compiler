CUR_DIR=$(pwd)

export CLEAN_HOME=$CUR_DIR/clean-cross
export CLM=$CLEAN_HOME/bin/clm
export CLEAN_STDLIB=$CLEAN_HOME/lib/StdLib

export STDENV_CLEAN_HOME=$CLEAN_HOME # Default use folder clean (compiled compiler).

export PATCH_MAKE=$CUR_DIR/make

export TARGET=ARMCROSS

# ANDROID or RASPBERRYPI
export CROSS_PLATFORM=ANDROID

# DLL or EXE
export TARGETTYPE=DLL

export CC=/usr/bin/gcc
export AS=as
export LD=/usr/bin/ld
export CFLAGS=''

#
# Android
#
export NDK=/opt/android-ndk/android-ndk-r13b/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64
export NDK_TOOLCHAIN=${NDK}/bin
export CROSS_COMPILE=arm-linux-androideabi
export SYSROOT=/opt/android-ndk/android-ndk-r13b/platforms/android-21/arch-arm/

export CROSS_CC=${NDK_TOOLCHAIN}/${CROSS_COMPILE}-gcc
export CROSS_AS=${NDK_TOOLCHAIN}/${CROSS_COMPILE}-as
export CROSS_LD=${NDK_TOOLCHAIN}/${CROSS_COMPILE}-ld
export CROSS_CFLAGS="--sysroot=${SYSROOT} -pie -march=armv7-a -msoft-float -DPIC -DANDROID"
export CROSS_ASFLAGS="--defsym PIC=1"
export CROSS_LDFLAGS=""

sh build-target.sh

