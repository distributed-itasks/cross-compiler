# Patch

# Patch Code-generator Linux64 GCC support

(cd code-generator; sed -i 's/\(!rts_got_flag\)/\(pic_flag \&\& !rts_got_flag\)/g' cgaas.c)

# Force PIC code generation for Android
if [ "$CROSS_PLATFORM" = "ANDROID" ]; then
	(cd code-generator; sed -i 's/int\ pic_flag=0;/int\ pic_flag=1;/g' cg.c)
fi

# End patch

(cd "libraries/ArgEnvUnix"; make -f Makefile ArgEnvC.o CC=$CC COPTIONS=$CFLAGS)

if test ! -d $CLEAN_HOME/Temp ; then
	mkdir $CLEAN_HOME/Temp
	touch $CLEAN_HOME/Temp/errors
fi

sh build-tools.sh
sh build-code-generator.sh
sh build-runtimesystem.sh
sh build-compiler.sh
sh build-libraries.sh
sh build-linker.sh
sh build-clean-itasks.sh #sh build-clean.sh
sh build-stdenv.sh
sh build-batchbuild.sh
sh build-sapl.sh
sh build-platform.sh
sh build-itasks.sh
sh build-cpm.sh

mkdir -p clean/Temp
touch clean/Temp/errors

# Remove _startupTrace from OBJECT_MODULES.
(cd clean/StdEnv; sed -i 's/OBJECT_MODULES="_startup _startupTrace"/OBJECT_MODULES="_startup"/g' install.sh)

# Fix CreateThread for Win32
(cd clean/data/iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Windows/System; sed -i 's/ccall CreateThread/ccall CreateThread@24/g' _WinBase.icl)
