mkdir -p clean
#cp tools/CleanIDE/CleanLicenseConditions.txt clean
cp tools/build/Linux/txt/Makefile clean
cp tools/build/Linux/txt/README clean

mkdir -p clean/bin
cp tools/clm/clm clean/bin
cp tools/clm/patch_bin clean/bin
cp tools/htoclean/htoclean\ source\ code/htoclean clean/bin

mkdir -p clean/exe
cp compiler/cocl clean/exe
cp code-generator/cg clean/exe
cp tools/elf_linker/linker clean/exe

mkdir -p clean/doc
# Doc folder can't be empty, then make will fail.
touch clean/doc/EMPTY

mkdir -p clean/man/man1
cp tools/build/Linux/txt/clm.1 clean/man/man1

mkdir -p clean/data/ArgEnv
for f in ArgEnvC.c  ArgEnv.dcl printenv.icl ArgEnv.icl Makefile README ; do
	cp "libraries/ArgEnvUnix/$f" "clean/data/ArgEnv/$f"
done

mkdir -p clean/data/Generics
cp libraries/GenLib/*.[id]cl clean/data/Generics/

mkdir -p clean/data/Gast
cp libraries/Gast/*.[id]cl clean/data/Gast/

mkdir -p clean/data/StdLib
cp libraries/StdLib/*.[id]cl clean/data/StdLib/

mkdir -p "clean/data/Directory/Clean System Files"
cp libraries/Directory/* clean/data/Directory
cp "libraries/Directory/Clean System Files Unix"/* "clean/data/Directory/Clean System Files"

mkdir -p "clean/data/MersenneTwister/Clean System Files"
cp libraries/MersenneTwister/*.* clean/data/MersenneTwister

mkdir -p "clean/data/TCPIP/Clean System Files"
cp libraries/TCPIP/TCPIP.dcl clean/data/TCPIP
cp libraries/TCPIP/TCPIP.icl clean/data/TCPIP
cp libraries/TCPIP/TCPDef.icl clean/data/TCPIP
cp libraries/TCPIP/TCPDef.dcl clean/data/TCPIP
cp libraries/TCPIP/TCPEvent.dcl	clean/data/TCPIP
cp libraries/TCPIP/TCPEvent.icl clean/data/TCPIP
cp libraries/TCPIP/TCPChannelClass.dcl clean/data/TCPIP
cp libraries/TCPIP/TCPChannelClass.icl clean/data/TCPIP
cp libraries/TCPIP/TCPChannels.dcl clean/data/TCPIP
cp libraries/TCPIP/TCPChannels.icl clean/data/TCPIP
cp libraries/TCPIP/TCPStringChannels.dcl clean/data/TCPIP
cp libraries/TCPIP/TCPStringChannels.icl clean/data/TCPIP
cp libraries/TCPIP/TCPStringChannelsInternal.dcl clean/data/TCPIP
cp libraries/TCPIP/TCPStringChannelsInternal.icl clean/data/TCPIP
cp libraries/TCPIP/tcp_bytestreams.dcl clean/data/TCPIP
cp libraries/TCPIP/tcp_bytestreams.icl clean/data/TCPIP
cp libraries/TCPIP/Linux_C/tcp.dcl clean/data/TCPIP
cp libraries/TCPIP/Linux_C/tcp.icl clean/data/TCPIP
cp libraries/TCPIP/Linux_C/ostcp.dcl clean/data/TCPIP
cp libraries/TCPIP/Linux_C/ostcp.icl clean/data/TCPIP
cp libraries/TCPIP/Linux_C/cTCP_121.o "clean/data/TCPIP/Clean System Files"
cp libraries/TCPIP/Linux_C/cTCP_121.c "clean/data/TCPIP/Clean System Files"
cp libraries/TCPIP/Linux_C/cTCP_121.h "clean/data/TCPIP/Clean System Files"

mkdir -p "clean/data/Dynamics/Clean System Files"
cp libraries/StdDynamicEnv/extension/StdCleanTypes.dcl clean/data/Dynamics
cp libraries/StdDynamicEnv/extension/StdCleanTypes.icl clean/data/Dynamics
cp libraries/StdDynamicEnv/extension/StdDynamic.dcl clean/data/Dynamics
cp libraries/StdDynamicEnv/extension/StdDynamicNoLinker.icl clean/data/Dynamics/StdDynamic.icl
cp libraries/StdDynamicEnv/implementation/_SystemDynamic.dcl clean/data/Dynamics
cp libraries/StdDynamicEnv/implementation/_SystemDynamic.icl clean/data/Dynamics
