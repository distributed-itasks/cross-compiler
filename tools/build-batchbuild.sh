echo " ---- BUILD BATCHBUILD --- "

if [ "$TARGET" = "MACOS" ]; then
	(cd cleanide/BatchBuild ; export PATH=$CLEAN_HOME/bin:$PATH ; sh make.macosx.sh)
else
	(cd cleanide/BatchBuild ; export PATH=$CLEAN_HOME/bin:$PATH ; sh make.linux.sh)
fi

mkdir -p clean
cp cleanide/BatchBuild/batch_build clean-cross/
if [ "$TARGET" = "MACOS" ]; then
	cp tools/build/MacOSX/IDEEnvs clean-cross/IDEEnvs
elif [ "$CROSS_PLATFORM" = "LINUX64" ]; then
	cp tools/build/Linux/IDEEnvs clean-cross/IDEEnvs
else
	cp tools/build/Linux/IDEEnvs32 clean-cross/IDEEnvs
fi
