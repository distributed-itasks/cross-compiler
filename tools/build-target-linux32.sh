# -----------------------------------------------------------------------------
#  Clean Compiler for Rasberry Pi
#
#  This script builds a minimal Clean Compiler that can be used on the 
#  Rasberry Pi to compile the Clean compiler.
# -----------------------------------------------------------------------------
CUR_DIR=$(pwd)

export CLEAN_HOME=$CUR_DIR/clean-cross
export CLM=$CLEAN_HOME/bin/clm
export CLEAN_STDLIB=$CLEAN_HOME/lib/StdLib

export STDENV_CLEAN_HOME=$CLEAN_HOME # Default use folder clean (compiled compiler).

export PATCH_MAKE=$CUR_DIR/make

export TARGET=LINUX32

# ANDROID or RASPBERRYPI
export CROSS_PLATFORM=LINUX32

# DLL or EXE
export TARGETTYPE=EXE

export CC=/usr/bin/gcc
export AS=as
export LD=/usr/bin/ld
export CFLAGS=''

export CROSS_CC=/usr/bin/gcc
export CROSS_AS=/usr/bin/as
export CROSS_LD=/usr/bin/ld
export CROSS_CFLAGS='-m32'
export CROSS_ASFLAGS="--32"
export CROSS_LDFLAGS="-melf_i386"

sh build-target.sh

