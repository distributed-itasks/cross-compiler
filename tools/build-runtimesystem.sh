# -----------------------------------------------------------------------------
#  Build Clean tools
#  Based on: https://svn.cs.ru.nl/repos/clean-tools/trunk/build/Linux
# -----------------------------------------------------------------------------

echo " ---- BUILD RUNTIMESYSTEM ---- "

if test ! -d runtimesystem ; then
	svn checkout https://svn.cs.ru.nl/repos/clean-run-time-system/trunk/ runtimesystem
fi

if [ "$CROSS_PLATFORM" = "ANDROID" ]; then
	if [ "$TARGETTYPE" = "DLL" ]; then
		(cd "runtimesystem"; make -f $PATCH_MAKE/runtimesystem/Makefile.android_armdll CC=$CROSS_CC AS=$CROSS_AS CFLAGS="$CROSS_CFLAGS" LD=$CROSS_LD ASFLAGS="$CROSS_ASFLAGS")
	else
		(cd "runtimesystem"; make -f $PATCH_MAKE/runtimesystem/Makefile.android_arm CC=$CROSS_CC AS=$CROSS_AS CFLAGS="$CROSS_CFLAGS" LD=$CROSS_LD ASFLAGS="$CROSS_ASFLAGS")
	fi
elif [ "$CROSS_PLATFORM" = "LINUX32" ]; then
	(cd "runtimesystem"; make -f $PATCH_MAKE/runtimesystem/Makefile.linux32 CC=$CROSS_CC AS=$CROSS_AS CFLAGS="$CROSS_CFLAGS" LD=$CROSS_LD ASFLAGS="$CROSS_ASFLAGS" LDFLAGS="$CROSS_LDFLAGS")	
elif [ "$CROSS_PLATFORM" = "LINUX64" ]; then
	(cd "runtimesystem"; $PATCH_MAKE/runtimesystem/make_linux64.sh)	
elif [ "$CROSS_PLATFORM" = "MACOS" ]; then
	(cd "runtimesystem/macho64"; sh make.sh) 
elif [ "$CROSS_PLATFORM" = "WIN32" ]; then
	(cd runtimesystem; $CLM -I ../libraries/ArgEnvUnix fixgnuasobj -o fixgnuasobj)
	cp $PATCH_MAKE/runtimesystem/win/_startup0.o runtimesystem
	cp $PATCH_MAKE/runtimesystem/win/build_windows_object_files.sh runtimesystem
	(cd "runtimesystem"; sh build_windows_object_files.sh)
elif [ "$CROSS_PLATFORM" = "WIN64" ]; then
	cp $PATCH_MAKE/runtimesystem/win64/*.o runtimesystem
	cp $PATCH_MAKE/runtimesystem/win/build_windows_object_files_64.sh runtimesystem
else
	(cd "runtimesystem"; make -f $PATCH_MAKE/runtimesystem/Makefile.linux_arm CC=$CROSS_CC AS=$CROSS_AS CFLAGS="$CROSS_CFLAGS" LD=$CROSS_LD)
fi
