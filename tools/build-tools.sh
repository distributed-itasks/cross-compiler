(cd "tools/clm"; make -f $PATCH_MAKE/clm/Makefile.linux_arm clm CC=$CC CFLAGS=$CFLAGS)
if [ "$TARGET" = "ARM" ]; then
	(cd "tools/clm"; make -f $PATCH_MAKE/clm/Makefile.linux_arm patch_bin CC=$CC CFLAGS=$CFLAGS)
elif [ "$TARGET" = "MACOS" ]; then
	(cd "tools/clm"; make -f Makefile.macho64 clm)
	(cd "tools/clm"; make -f Makefile.macho64 patch_bin)
else
	(cd "tools/clm"; make -f $PATCH_MAKE/clm/Makefile.linux patch_bin CC=$CC CFLAGS=$CFLAGS)
fi

# Copy Clean.h to a global include directory.
if test ! -d inclde ; then
	mkdir include
fi

cp tools/htoclean/Clean.h include/Clean.h

# Build htoclean
(cd tools/htoclean/htoclean\ source\ code/; $CLM -I unix -I ../../../libraries/ArgEnvUnix -h 4m -nt -nr htoclean -o htoclean)




