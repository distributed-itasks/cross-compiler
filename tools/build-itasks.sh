echo " ---- BUILD ITASKS ---- "

find . -iname ".git" | xargs rm -rf

rm -r "iTasks-SDK/Dependencies/graph_copy"
cp -r clean-graph-copy iTasks-SDK/Dependencies/graph_copy

if [ "$CROSS_PLATFORM" = "LINUX32" ]; then
	rm -r "iTasks-SDK/Dependencies/graph_copy/linux32/Clean System Files/*"
	(cd iTasks-SDK/Dependencies/graph_copy/linux32 ; make -f Makefile CC=$CROSS_CC CFLAGS="$CROSS_CFLAGS" AS=$CROSS_AS ASFLAGS=$CROSS_ASFLAGS)
elif [ "$CROSS_PLATFORM" = "LINUX64" ]; then
	rm -r "iTasks-SDK/Dependencies/graph_copy/linux64/Clean System Files/*"
	(cd iTasks-SDK/Dependencies/graph_copy/linux64 ; make -f Makefile CC=$CROSS_CC CFLAGS="$CROSS_CFLAGS" AS=$CROSS_AS ASFLAGS=$CROSS_ASFLAGS)
elif [ "$CROSS_PLATFORM" = "WIN32" ]; then
	rm -r "iTasks-SDK/Dependencies/graph_copy/win32/Clean System Files/*"
	(cd iTasks-SDK/Dependencies/graph_copy/win32 ; make -f Makefile CC=$CROSS_CC CFLAGS="$CROSS_CFLAGS" AS=$CROSS_AS ASFLAGS=$CROSS_ASFLAGS)
elif [ "$CROSS_PLATFORM" = "WIN64" ]; then
	rm -r "iTasks-SDK/Dependencies/graph_copy/win32/Clean System Files/*"
	cp -r * $PATCH_MAKE/graph_copy/win64 iTasks-SDK/Dependencies/graph_copy/win32/
elif [ "$CROSS_PLATFORM" = "MACOS" ]; then
	rm -r "iTasks-SDK/Dependencies/graph_copy/macosx/Clean System Files/*"
	(cd iTasks-SDK/Dependencies/graph_copy/macosx ; make -f Makefile CC=$CROSS_CC CFLAGS="$CROSS_CFLAGS" AS=$CROSS_AS ASFLAGS=$CROSS_ASFLAGS)
else
	(cd iTasks-SDK/Dependencies/graph_copy/arm32 ; make -f Makefile CC=$CROSS_CC CFLAGS="$CROSS_CFLAGS" AS=$CROSS_AS ASFLAGS="$CROSS_ASFLAGS")
fi

# End graph_copy

if [ "$CROSS_PLATFORM" = "WIN32" ]; then
	cp platform/src/libraries/OS-Independent/Text/Unicode/bsearch.o "iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Windows-32/Clean System Files/bsearch.o"
	cp platform/src/libraries/OS-Independent/Text/Unicode/WCsubst.o "iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Windows-32/Clean System Files/WCsubst.o"
elif [ "$CROSS_PLATFORM" = "WIN64" ]; then
	cp platform/src/libraries/OS-Independent/Text/Unicode/bsearch.o "iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Windows-64/Clean System Files/bsearch.o"
	cp platform/src/libraries/OS-Independent/Text/Unicode/WCsubst.o "iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Windows-64/Clean System Files/WCsubst.o"
elif [ "$CROSS_PLATFORM" = "LINUX64" ]; then
	cp platform/src/libraries/OS-Independent/Text/Unicode/bsearch.o "iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Linux-64/Clean System Files/bsearch.o"
	cp platform/src/libraries/OS-Independent/Text/Unicode/WCsubst.o "iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Linux-64/Clean System Files/WCsubst.o"	
else
	cp platform/src/libraries/OS-Independent/Text/Unicode/bsearch.o "iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Linux-32/Clean System Files/bsearch.o"
	cp platform/src/libraries/OS-Independent/Text/Unicode/WCsubst.o "iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Linux-32/Clean System Files/WCsubst.o"
fi

cp -r iTasks-SDK/Installation/Files/Sapl clean/StdEnv
cp -r iTasks-SDK clean/data/iTasks-SDK

if [ "$TARGET" = "ARMCROSS" ] || [ "$TARGET" = "ARMCROSS" ]; then
	cp make/platform/_Pointer.icl clean/data/iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Independent/System/_Pointer.icl
fi

if [ "$CROSS_PLATFORM" = "ANDROID" ]; then
	cp make/platform/OS-Linux/System/_Posix.dcl clean/data/iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Linux/System/_Posix.dcl
	cp make/platform/OS-Linux/System/_Posix.icl clean/data/iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Linux/System/_Posix.icl
	cp make/platform/OS-Posix/System/Time.icl clean/data/iTasks-SDK/Dependencies/clean-platform/src/libraries/OS-Posix/System/Time.icl
fi

mkdir -p clean/etc
if [ "$CROSS_PLATFORM" = "ANDROID" ]; then
	cp $PATCH_MAKE/support/IDEEnvs-Android clean/etc/IDEEnvs
elif [ "$CROSS_PLATFORM" = "LINUX32" ]; then
	cp $PATCH_MAKE/support/IDEEnvs-Linux32 clean/etc/IDEEnvs
elif [ "$CROSS_PLATFORM" = "LINUX64" ]; then
	cp $PATCH_MAKE/support/IDEEnvs-Linux64 clean/etc/IDEEnvs
elif [ "$CROSS_PLATFORM" = "WIN32" ]; then
	cp $PATCH_MAKE/support/IDEEnvs-Win32 clean/etc/IDEEnvs
elif [ "$CROSS_PLATFORM" = "WIN64" ]; then
	cp $PATCH_MAKE/support/IDEEnvs-Win64 clean/etc/IDEEnvs
elif [ "$CROSS_PLATFORM" = "MACOS" ]; then
	cp $PATCH_MAKE/support/IDeenvs-MacOs clean/etc/IDEEnvs
else
	cp $PATCH_MAKE/support/IDEEnvs-RBP clean/etc/IDEEnvs
fi

echo "----- iTasks DONE -----" 
