echo " ---- BUILD SAPL ---- "

if [ "$TARGET" = "MACOS" ]; then
	(cd sapldynamics ; $CLEAN_HOME/batch_build SaplCollectorLinkerMacOSX.prj)
else
	(cd sapldynamics ; $CLEAN_HOME/batch_build SaplCollectorLinkerLinux32.prj)
fi
cp sapldynamics/sapl-collector-linker clean/exe
echo 'cp -R Sapl $INSTALL_STDENV_DIR' >> clean/StdEnv/install.sh


