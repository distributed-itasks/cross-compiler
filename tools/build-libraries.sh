echo " ---- BUILD LIBARIES ---- "

CUR_DIR=$(pwd)
INCLUDE_DIR=${CUR_DIR}/include

if [ "$TARGET" = "WIN32" ] || [ "$TARGET" = "WIN64" ] ; then
	rm "libraries/Directory/Clean System Files Windows/*.o"
	rm "libraries/Directory/Clean System Files Windows/*.obj"
	(cd "libraries/Directory/Clean System Files Windows"; $CROSS_CC $CROSS_CFLAGS -I $INCLUDE_DIR -c -O cDirectory.c)
else
	(cd "libraries/Directory/Clean System Files Unix"; $CROSS_CC $CROSS_CFLAGS -I $INCLUDE_DIR -c -O cDirectory.c)
fi

if [ "$TARGET" = "WIN32" ] || [ "$TARGET" = "WIN64" ]; then
	(cd "libraries/TCPIP/Windows_C"; $CROSS_CC $CROSS_CFLAGS -I $INCLUDE_DIR -c -O cTCP_121.c)
else
	(cd "libraries/TCPIP/Linux_C"; $CROSS_CC $CROSS_CFLAGS -I $INCLUDE_DIR -c -O cTCP_121.c)
	(cd "libraries/TCPIP"; sed 's/, library "wsock_library"//' <ostcp.icl >ostcp.icl_; rm -f ostcp.icl; mv ostcp.icl_ ostcp.icl)
	(cd "libraries/TCPIP/Linux_C"; sed 's/, library "wsock_library"//' <ostcp.icl >ostcp.icl_; rm -f ostcp.icl; mv ostcp.icl_ ostcp.icl)
fi

if test ! -d libraries/StdDynamicEnv ; then
	svn checkout https://svn.cs.ru.nl/repos/clean-dynamic-system/trunk/dynamics/StdDynamicEnv/ libraries/StdDynamicEnv
fi

cp -r tools/build/Linux/Gast libraries/Gast
rm -rf libraries/Gast/.svn
