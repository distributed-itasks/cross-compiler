echo " ---- CODE GENERATOR ---- "

# cp make/cg.c code-generator/cg.c
if [ "$CROSS_PLATFORM" = "ANDROID" ]; then
	(cd code-generator; make -f $PATCH_MAKE/code-generator/Makefile.android_arm cg CC=$CC CFLAGS="$CFLAGS -m32 -DGNU_C -DLINUX -DLINUX_ELF -DARM -O -fomit-frame-pointer -I. -I$PATCH_MAKE/code-generator/includeandroid")
elif [ "$CROSS_PLATFORM" = "LINUX32" ]; then
	# Create always for 32 bits (fix for correct code)
	(cd code-generator; make -f $PATCH_MAKE/code-generator/Makefile.linux cg CC=$CC CFLAGS="$CFLAGS -m32 -DI486 -DGNU_C -DLINUX -DLINUX_ELF -O -fomit-frame-pointer")
elif [ "$CROSS_PLATFORM" = "LINUX64" ]; then
	(cd code-generator; make -f $PATCH_MAKE/code-generator/Makefile.linux64 cg CC=$CC CFLAGS="$CFLAGS -m64 -DI486 -DGNU_C -DLINUX -DLINUX_ELF -DG_AI64 -O -fomit-frame-pointer")
elif [ "$CROSS_PLATFORM" = "WIN32" ]; then
	# Build code-generator for i386 so that it will generate correct code (i386 code).
	(cd code-generator; make -f $PATCH_MAKE/code-generator/Makefile.win cg CC=$CC CFLAGS="$CFLAGS -m32 -DI486 -DGNU_C -D_WINDOWS_ -O -fomit-frame-pointer")
elif [ "$CROSS_PLATFORM" = "WIN64" ]; then
	(cd code-generator; make -f $PATCH_MAKE/code-generator/Makefile.linux64 cg CC=$CC CFLAGS="$CFLAGS -m64 -DI486 -DGNU_C -DLINUX -DLINUX_ELF -DG_AI64 -O -fomit-frame-pointer")
elif [ "$CROSS_PLATFORM" = "MACOS" ]; then
	cp $PATCH_MAKE/code-generator/cgaas.c code-generator/cgaas.c
	(cd code-generator; make -f Makefile.macosx64 cg)
elif [ "$CROSS_PLATFORM" = "RASPBERRYPI" ]; then
	 (cd code-generator; make -f $PATCH_MAKE/code-generator/Makefile.linux_arm cg CC=$CC CFLAGS="$CFLAGS -m32 -DGNU_C -DLINUX -DLINUX_ELF -DARM -O -fomit-frame-pointer")
else
	(cd code-generator; make -f $PATCH_MAKE/code-generator/Makefile.linux_arm cg CC=$CC CFLAGS="$CFLAGS -DGNU_C -DLINUX -DLINUX_ELF -DARM -O -fomit-frame-pointer")
fi
