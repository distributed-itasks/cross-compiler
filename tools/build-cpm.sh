echo " ---- BUILD CPM ---- "

if [ "$TARGET" = "ARM" ] || [ "$TARGET" = "ARMCROSS" ]; then
	if [ "$TARGETTYPE" = "DLL" ]; then
		cp make/PmCleanSystemShared.icl cleanidecpm/Unix/PmCleanSystem.icl
	else
		cp make/PmCleanSystem.icl cleanidecpm/Unix/PmCleanSystem.icl
	fi
elif [ "$TARGET" = "WIN32" ]; then
	cp make/PmCleanSystem_win32.icl cleanidecpm/Unix/PmCleanSystem.icl
elif [ "$TARGET" = "WIN64" ]; then
	cp make/PmCleanSystem_win64.icl cleanidecpm/Unix/PmCleanSystem.icl
elif [ "$TARGET" = "LINUX32" ]; then
	cp make/PmCleanSystem_linux32.icl cleanidecpm/Unix/PmCleanSystem.icl
elif [ "$CROSS_PLATFORM" = "LINUX64" ]; then
	cp make/PmCleanSystem_linux64.icl cleanidecpm/Unix/PmCleanSystem.icl
fi

if [ "$TARGET" = "MACOS" ]; then
	rm -r clean-cross/lib/Directory
	mkdir -p "clean-cross/lib/Directory/Clean System Files"
	cp libraries/Directory/* clean-cross/lib/Directory
	cp "libraries/Directory/Clean System Files Unix"/* "clean-cross/lib/Directory/Clean System Files"
	(cd cleanidecpm ; ../clean-cross/batch_build CpmMacOSX.prj)
else
	(cd cleanidecpm ; ../clean-cross/batch_build CpmLinux.prj)
fi
cp cleanidecpm/cpm/cpm clean/bin/cpm
