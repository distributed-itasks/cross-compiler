CUR_DIR=$(pwd)

export CLEAN_HOME=$CUR_DIR/clean-cross
export CLM=$CLEAN_HOME/bin/clm
export CLEAN_STDLIB=$CLEAN_HOME/lib/StdLib

export STDENV_CLEAN_HOME=$CLEAN_HOME # Default use folder clean (compiled compiler).

export PATCH_MAKE=$CUR_DIR/make

export TARGET=LINUX64

# ANDROID or RASPBERRYPI
export CROSS_PLATFORM=LINUX64

# DLL or EXE
export TARGETTYPE=EXE

export CC=/usr/bin/gcc
export AS=as
export LD=/usr/bin/ld
export CFLAGS=''

export CROSS_CC=/usr/bin/gcc
export CROSS_AS=/usr/bin/as
export CROSS_LD=/usr/bin/ld
export CROSS_CFLAGS=''
export CROSS_ASFLAGS=""
export CROSS_LDFLAGS=""

sh build-target.sh

