#!/bin/sh


(cd backendC/CleanCompilerSources; make CC=$CC CFLAGS="$CFLAGS -D_SUN_ -DGNU_C -O -fomit-frame-pointer" AS=$AS LD=$LD)
(cd main/Unix; make -f Makefile all CC=$CC CFLAGS="$CFLAGS -pedantic -Wall -W -O");
if [ "$TARGET" = "ARM" ]; then
	$CLM -h 128m -s 16m -nt -nw -ci -ns -nr -I backend -I frontend -I main -I main/Unix \
		-IL ArgEnv \
		-l backendC/CleanCompilerSources/backend.a \
		cocl -o cocl
else
	$CLM -h 128m -s 16m -nt -nw -ci -ns -nr -I backend -I frontend -I main -I main/Unix \
		-IL ArgEnv \
	 	-l main/Unix/cDirectory.o \
	 	-l main/Unix/ipc.o \
	 	-l main/Unix/set_return_code_c.o \
		-l backendC/CleanCompilerSources/backend.a \
		cocl -o cocl
fi

# 	-l main/Unix/cDirectory.o \
# 	-l main/Unix/ipc.o \
# 	-l main/Unix/set_return_code_c.o \
