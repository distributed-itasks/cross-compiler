FROM debian:8.6
MAINTAINER Arjan Oortgiese <a.oortgiese@student.ru.nl>

RUN apt-get update && apt-get install -qq -y build-essential curl subversion git libc6-dev-i386 mingw32 mingw-w64 lib32z1 unzip

# Setup Clean+iTasks (clean-cross)
RUN mkdir -p /opt/clean/tmp \
	&& (cd /opt/clean/tmp ; mkdir clean-cross ; curl -sL ftp://ftp.cs.ru.nl/pub/Clean/nightly/clean-itasks-linux64-20160502.tar.gz | tar -xz -C ./clean-cross --strip-components=1)
COPY ./*.sh /opt/clean/tmp/
COPY ./tools /opt/clean/tmp/tools/
COPY ./support /opt/clean/tmp/support/

# Get source code.
RUN mkdir -p /opt/clean/tmp/source \
	&& (cd /opt/clean/tmp/source ; sh ../get-sources.sh)

# Android
RUN apt-get install lib32stdc++6
RUN mkdir -p /opt/jdk8 \
	&& (cd /opt/jdk8 ; curl -sL --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u111-b14/jdk-8u111-linux-x64.tar.gz | tar --strip-components=1 -xz)

ENV JAVA_HOME /opt/jdk8

RUN ln -s ${JAVA_HOME}/bin/java /usr/bin/java
RUN ln -s ${JAVA_HOME}/bin/javac /usr/bin/javac

RUN mkdir -p /opt/android-ndk \
	&& (cd /opt/android-ndk ; curl -sL -o ndk.zip https://dl.google.com/android/repository/android-ndk-r13b-linux-x86_64.zip) \
	&& (cd /opt/android-ndk ; unzip ndk.zip -d ./) \
	&& rm /opt/android-ndk/ndk.zip

RUN mkdir -p /opt/android-sdk \
	&& (cd /opt/android-sdk ; curl -sL https://dl.google.com/android/android-sdk_r24.4.1-linux.tgz | tar --strip-components=1 -xz)

RUN echo y | /opt/android-sdk/tools/android update sdk --no-ui -a --filter tools,platform-tools,android-21,android-23,build-tools-23.0.2,extra-android-support,extra-android-m2repository

# Fix for: https://github.com/android-ndk/ndk/issues/188
RUN apt-get install -qq -y file

# Setup Raspberry Pi cross-compiler
RUN mkdir -p /opt/raspberrypi \
	&& (cd /opt/raspberrypi ; git clone https://github.com/raspberrypi/tools.git) \
	&& (cd /opt/raspberrypi/tools ; git reset --hard 6376a50ed059b3c852ce3836981ba5ff07d6b368)

# Linux 32
RUN (cd /opt/clean/tmp ; sh make-target-linux32.sh) \
	&& (cd /opt/clean/tmp/target/linux32 ; sh build-target-linux32.sh) \
	&& (mv /opt/clean/tmp/target/linux32/clean /opt/clean/linux32 ; cd /opt/clean/linux32 ; make) \
	&& (rm -r /opt/clean/tmp/target/linux32)

# Linux 64
RUN (cd /opt/clean/tmp ; sh make-target-linux64.sh) \
	&& (cd /opt/clean/tmp/target/linux64 ; sh build-target-linux64.sh) \
	&& (mv /opt/clean/tmp/target/linux64/clean /opt/clean/linux64 ; cd /opt/clean/linux64 ; make) \
	&& (rm -r /opt/clean/tmp/target/linux64)

# Raspberry Pi
RUN (cd /opt/clean/tmp ; sh make-target-raspberrypi.sh) \
	&& (cd /opt/clean/tmp/target/raspberrypi ; sh build-target-rbp.sh) \
	&& (mv /opt/clean/tmp/target/raspberrypi/clean /opt/clean/raspberrypi; cd /opt/clean/raspberrypi ; make) \
	&& (rm -r /opt/clean/tmp/target/raspberrypi)

# Android
RUN (cd /opt/clean/tmp ; sh make-target-android.sh) \
	&& (cd /opt/clean/tmp/target/android ; sh build-target-android.sh) \
	&& (mv /opt/clean/tmp/target/android/clean /opt/clean/android; cd /opt/clean/android ; make) \
	&& (rm -r /opt/clean/tmp/target/android)

# Windows 32
RUN (cd /opt/clean/tmp ; sh make-target-win.sh) \
	&& (cd /opt/clean/tmp/target/win32 ; sh build-target-win32.sh) \
	&& (mv /opt/clean/tmp/target/win32/clean /opt/clean/win32; cd /opt/clean/win32 ; make) \
	&& (rm -r /opt/clean/tmp/target/win32)

# Remove temp files.
RUN rm -rf /opt/clean/tmp

RUN mkdir -p /usr/src/app

COPY ./cpm /usr/bin/cpm
RUN chmod +x /usr/bin/cpm

WORKDIR /usr/src/app
