(cd source ; sh ../get-sources.sh)

mkdir -p target/macos

cp -r tools/* target/macos

cp -r source/* target/macos

cp -r clean-cross target/macos/clean-cross
(cd target/macos/clean-cross ; make)

cp -r support/Engine.* target/macos/iTasks-SDK/Server/iTasks/_Framework
