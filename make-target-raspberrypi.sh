(cd source ; sh ../get-sources.sh)

mkdir -p target/raspberrypi

cp -r tools/* target/raspberrypi

cp -r source/* target/raspberrypi

cp -r clean-cross target/raspberrypi/clean-cross
(cd target/raspberrypi/clean-cross ; make)

#rm -r target/rbp/code-generator
#mkdir -p target/rbp/code-generator
#cp support/code-generator-rdbp/* target/rbp/code-generator

cp -r support/Engine.* target/raspberrypi/iTasks-SDK/Server/iTasks/_Framework
