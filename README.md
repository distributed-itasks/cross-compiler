# Distributed Clean+iTasks cross-compilers

This repository contains scripts to compile Clean cross compilers for the distributed version of iTasks.

## Docker

Run `sudo docker build -t "cleanitasks:cross-compilers" .` to build the Docker image.

To compile a project run: `sudo docker run -v <project path>:/usr/src/app -v <android app path>:/usr/src/android-app cleanitasks:cross-compilers cpm <target> <project>.prj`

For `<target>` the options are:

* **linux32**
* **linux64**
* **android**
* **raspberrypi**
* **win32**

For the `<target>` android you need the [iTasks-Android project](https://gitlab.science.ru.nl/distributed-itasks/itasks-android).

## Using scripts

You can use the scripts when you do not want to use Docker or when you are using MacOS

### Preperation

Download Clean + iTasks from [http://clean.cs.ru.nl/Download_Clean](http://clean.cs.ru.nl/Download_Clean) and unpack it in clean-cross.

#### Preperation Linux

Install build-essential e.g. `apt-get install build-essential`
Install libc6-dev-i386 e.g. `apt-get install libc6-dev-i386`
Install lib32z1 e.g. `sudo apt-get install lib32z1`

For building a Windows 32 cross-compiler you also need to install mingw32 e.g. `apt-get install mingw32`

#### Preperation MacOS

Install developer tools: `xcode-select --install`

#### Building

Building example for MacOS:

```
mkdir sources
(cd sources ; sh ../get-sources.sh)
sh make-target-mac.sh
(cd target/macos ; sh build-target-macos.sh)
```
