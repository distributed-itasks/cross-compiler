if test ! -d libraries ; then
	svn checkout -r 1363 https://svn.cs.ru.nl/repos/clean-libraries/trunk/Libraries libraries
fi

if test ! -d platform ; then
	git clone https://gitlab.science.ru.nl/clean-and-itasks/clean-platform.git platform
fi

if test ! -d cleanide ; then
	svn checkout -r 538 https://svn.cs.ru.nl/repos/clean-ide/trunk/ cleanide
fi

if test ! -d code-generator ; then
	svn checkout -r 296 https://svn.cs.ru.nl/repos/clean-code-generator/trunk/ code-generator
fi

if test ! -d compiler ; then
	svn checkout -r 2690 https://svn.cs.ru.nl/repos/clean-compiler/branches/itask/ compiler
fi

if test ! -d cleanidecpm ; then
	svn checkout -r 538 https://svn.cs.ru.nl/repos/clean-ide/trunk/ cleanidecpm
fi

if test ! -d iTasks-SDK ; then
	git clone -b distributed https://gitlab.science.ru.nl/distributed-itasks/iTasks-SDK.git
	(cd iTasks-SDK ; git reset --hard 1043efa4abb1c052037242e8f669775d7de55408)
	(cd iTasks-SDK ; sh init_deps.sh)
fi

if test ! -d libraries/StdDynamicEnv ; then
	svn checkout -r 651 https://svn.cs.ru.nl/repos/clean-dynamic-system/trunk/dynamics/StdDynamicEnv/ libraries/StdDynamicEnv
fi

if test ! -d runtimesystem ; then
	svn checkout -r 387 https://svn.cs.ru.nl/repos/clean-run-time-system/trunk/ runtimesystem
fi

if test ! -d sapldynamics ; then
	svn checkout -r 655 https://svn.cs.ru.nl/repos/clean-dynamic-system/branches/itask sapldynamics
fi

if test ! -d tools ; then
	svn checkout -r 607 https://svn.cs.ru.nl/repos/clean-tools/trunk/ tools
fi

if test ! -d clean-graph-copy ; then
	git clone https://gitlab.science.ru.nl/clean-and-itasks/clean-graph-copy.git
	(cd clean-graph-copy; git reset --hard f6abbff0e65e4990cd91b06abda6ab376ed37582)
fi
