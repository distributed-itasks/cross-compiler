(cd source ; sh ../get-sources.sh)

mkdir -p target/win32

cp -r tools/* target/win32

cp -r source/* target/win32

cp -r clean-cross target/win32/clean-cross
(cd target/win32/clean-cross ; make)

cp -r support/Engine.* target/win32/iTasks-SDK/Server/iTasks/_Framework
