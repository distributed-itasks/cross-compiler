(cd source ; sh ../get-sources.sh)

mkdir -p target/linux32

cp -r tools/* target/linux32
cp -r source/* target/linux32

cp -r clean-cross target/linux32/clean-cross
(cd target/linux32/clean-cross ; make)

cp -r support/Engine.* target/linux32/iTasks-SDK/Server/iTasks/_Framework
